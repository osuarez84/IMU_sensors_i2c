/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */



/* ANOTACIONES: este proyecto utiliza la placa de pruebas STM32F446 Nucleo
 * para tomar datos de una unidad IMU GY-95 que tiene 9 DOF mediante la
 * utilizacion de 3 sensores:
 * Accelerometer
 * Gyroscope
 * Compass */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart2;


uint16_t x, y, z;
float xg, yg, zg;

uint8_t I2C_txBuffer[I2C_BUFFER_SIZE];
uint8_t I2C_rxBuffer[I2C_BUFFER_SIZE];

uint8_t USART_txBuffer[UART_BUFFER_SIZE];
uint8_t USART_rxBuffer[UART_BUFFER_SIZE];


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM6_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM7_Init(void);
static void MX_TIM1_Init(void);


/* Private function prototypes -----------------------------------------------*/
static void adxl345_read_values(uint8_t reg);

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();


  /* Configure the system clock */
  SystemClock_Config();



  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM6_Init();
  MX_I2C1_Init();
  MX_USART2_UART_Init();
  MX_TIM7_Init();
  MX_TIM1_Init();

  /* Accelerometer configuration -------------------------------------------- */
  /* clear power control register */
  I2C_txBuffer[0] = Power_Register;

  if(HAL_I2C_Mem_Write(&hi2c1, (uint16_t)ACC_I2C_add, Power_Register,
		   1, (uint8_t*)I2C_txBuffer, 1, 100) != HAL_OK) {
	  _Error_Handler(__FILE__, __LINE__);
  }


  HAL_Delay(5);

  /* Set Measure bit and wakeup frequency */
  I2C_txBuffer[0] = 0x08;
  if(HAL_I2C_Mem_Write(&hi2c1, (uint16_t)ACC_I2C_add, Power_Register,
		  1, (uint8_t*)I2C_txBuffer, 1, 100) != HAL_OK) {
	  _Error_Handler(__FILE__, __LINE__);
  }

  HAL_Delay(5);

  /* adjust data format = +-4g*/
  I2C_txBuffer[0] = 0x01;
  if(HAL_I2C_Mem_Write(&hi2c1, (uint16_t)ACC_I2C_add, Data_Format_Reg,
		  1, (uint8_t*)I2C_txBuffer, 1, 100) != HAL_OK) {
	  _Error_Handler(__FILE__, __LINE__);
  }

  HAL_Delay(5);

  /* read device ID to check everything is ok */
  if(HAL_I2C_Mem_Read(&hi2c1, (uint16_t)ACC_I2C_add, DEV_ID_Reg,
		  1, (uint8_t*)I2C_rxBuffer, 1, 100) != HAL_OK){
	  _Error_Handler(__FILE__, __LINE__);
  }

  HAL_Delay(5);

  /* LED ON if everything is ok */
  if(I2C_rxBuffer[0] == 0xE5){
	  HAL_Delay(500);
	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 1);
	  HAL_Delay(500);
	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 0);
  }


  /* Reset the rx buffer */
  for(uint8_t i = 0; i < I2C_BUFFER_SIZE; i++){
	  I2C_rxBuffer[i] = 0;
  }

  /* Gyroscope config ------------------------------------------------------- */
  /* power management set to:
   * clock select = internal oscillator
   * no reset, no sleep mode
   * no standby mode
   * sample rate to = 125Hz
   * parameter to +- 2000 degrees/s
   * low pass filter = 5Hz
   * no interrupt */

//  I2C_txBuffer[0] = 0x00;
//  if(HAL_I2C_Mem_Write(&hi2c1, (uint16_t)GYR_I2C_add, PWR_MGM,
//		  1, (uint8_t*)I2C_txBuffer, 1, 100) != HAL_OK) {
//	  _Error_Handler(__FILE__, __LINE__);
//  }
//  HAL_Delay(5);
//
//  I2C_txBuffer[0] = 0x07;
//  if(HAL_I2C_Mem_Write(&hi2c1, (uint16_t)GYR_I2C_add, SMPLRT_DIV,
//		  1, (uint8_t*)I2C_txBuffer, 1, 100) != HAL_OK) {
//	  _Error_Handler(__FILE__, __LINE__);
//  }
//  HAL_Delay(5);
//
//  I2C_txBuffer[0] = 0x1E;
//  if(HAL_I2C_Mem_Write(&hi2c1, (uint16_t)GYR_I2C_add, DLPF_FS,
//		  1, (uint8_t*)I2C_txBuffer, 1, 100) != HAL_OK) {
//	  _Error_Handler(__FILE__, __LINE__);
//  }
//  HAL_Delay(5);
//
//  I2C_txBuffer[0] = 0x00;
//  if(HAL_I2C_Mem_Write(&hi2c1, (uint16_t)GYR_I2C_add, INT_CFG,
//		  1, (uint8_t*)I2C_txBuffer, 1, 100) != HAL_OK) {
//	  _Error_Handler(__FILE__, __LINE__);
//  }
//  HAL_Delay(5);
//
//  /* read device ID to check everything is ok */
//  if(HAL_I2C_Mem_Read(&hi2c1, (uint16_t)GYR_I2C_add, WHO_AM_I,
//		  1, (uint8_t*)I2C_rxBuffer, 1, 100) != HAL_OK){
//	  _Error_Handler(__FILE__, __LINE__);
//  }
//  HAL_Delay(5);
//
//  /* check access to the device */
//  if(I2C_rxBuffer[0] == GYR_I2C_add){
//	  HAL_Delay(500);
//	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 1);
//	  HAL_Delay(500);
//	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 0);
//  }
//

  /* Compass config ---------------------------------------------------- */
  /* read device ID to check everything is ok */
  if(HAL_I2C_Mem_Read(&hi2c1, (uint16_t)COM_I2C_add, Identification_Reg_A,
		  1, (uint8_t*)I2C_rxBuffer, 1, 100) != HAL_OK){
	  _Error_Handler(__FILE__, __LINE__);
  }
  HAL_Delay(5);

  /* check access to the device */
  if(I2C_rxBuffer[0] == 0b00010010){
	  HAL_Delay(500);
	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 1);
	  HAL_Delay(500);
	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 0);
  }



  while (1)
  {
	  // Read values from accelerometer!
	  adxl345_read_values(DATAXYZ_REGS);

	  x = ((I2C_rxBuffer[1]<<8) | I2C_rxBuffer[0]);
	  y = ((I2C_rxBuffer[3]<<8) | I2C_rxBuffer[2]);
	  z = ((I2C_rxBuffer[5]<<8) | I2C_rxBuffer[4]);

	  /* compute real G using factor scale for the scale selected: 4g */
	  xg = x * G4_FACTOR_SCALE;
	  yg = y * G4_FACTOR_SCALE;
	  zg = z * G4_FACTOR_SCALE;

	  HAL_Delay(100);

	  // Read values from gyroscope
	  // TODO

	  HAL_Delay(100);
  }


}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM6 init function */
static void MX_TIM6_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;

  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 0;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 0;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM7 init function */
static void MX_TIM7_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;

  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 0;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 0;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

static void adxl345_read_values(uint8_t reg){

	if(HAL_I2C_Mem_Read(&hi2c1, ACC_I2C_add, reg, 1, (uint8_t*)I2C_rxBuffer, 6, 100) != HAL_OK){
		_Error_Handler(__FILE__, __LINE__);
	}

}




void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *I2cHandle)
{
	// TODO
}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c){
	// TODO
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
