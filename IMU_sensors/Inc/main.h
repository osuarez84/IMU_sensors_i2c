/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB


#define I2C_BUFFER_SIZE		16
#define UART_BUFFER_SIZE 	16

/* Accelerometer ------------------------------------------------------------ */
/* Register addresses for ADXL345 accelerometer */
#define ACC_I2C_add			0x53<<1
#define DEV_ID_Reg			0x00
#define Power_Register		0x2D
#define DATAXYZ_REGS		0x32
#define Data_Format_Reg		0x31
#define X_Axis_Reg_DATAX0	0x32
#define X_Axis_Reg_DATAX1	0x33
#define Y_Axis_Reg_DATAY0	0x34
#define Y_Axis_Reg_DATAY1	0x35
#define Z_Axis_Reg_DATAZ0	0x36
#define Z_Axis_Reg_DATAZ1	0x37

#define G4_FACTOR_SCALE		0.0078



/* Gyroscope ---------------------------------------------------------------- */
#define GYR_I2C_add			0x69<<1

/* Registers */
#define WHO_AM_I			0x00	// R/W
#define SMPLRT_DIV			0x15	// R/W
#define DLPF_FS				0x16	// R/W
#define INT_CFG				0x17	// R/W
#define INT_STATUS			0x1A	// R
#define TEMP_OUT_H			0x1B	// R
#define TEMP_OUT_L			0x1C	// R
#define GYRO_XOUT_H			0x1D	// R
#define GYRO_XOUT_L			0x1E	// R
#define GYRO_YOUT_H			0x1F	// R
#define GYRO_YOUT_L			0x20	// R
#define GYRO_ZOUT_H			0x21	// R
#define GYRO_ZOUT_L			0x22	// R
#define PWR_MGM				0x3E	// R/W


/* Compass ------------------------------------------------------------- */
#define COM_I2C_add				0x1E<<1

#define Identification_Reg_A	0x10	// R
/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
